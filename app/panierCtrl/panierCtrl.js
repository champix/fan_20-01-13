(function() {
    'use strict';

    angular
        .module('app')
        .controller('panierCtrl', ControllerController);

    ControllerController.$inject = ['$scope','produitSrvc'];
    function ControllerController($scope,produitSrvc) {
        var vm = this;
        this.panier=produitSrvc.panier;
        this.totals=produitSrvc.totals;
        this.total2=produitSrvc.total2;
        var removeOne=function (produit) {
            if(produit.quant>1)produit.quant-=1;
            else this.deleteProduitOnCart(produit);
        }
        this.deleteProduitOnCart=function(produit)
        {
            console.log(produit);
        }
        $scope.removeQuant=function (produit) {
            removeOne(produit);
        }
        $scope.addQuantOnProduct=produitSrvc.addProductToCart;



     
    }
})();