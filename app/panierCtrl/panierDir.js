(function() {
    'use strict';

    angular
        .module('app')
        // .controller('panierDirCtrl',['$scope','panierCtrl',function ($scope,panierCtrl) {
        //     this.makeReduc=function (params) {
        //        panierCtrl.total=panierCtrl.total-this.reduction 
        //     }
        // }])
        
      
      angular.module('app')  .directive('monPanier', Directive);

  //  Directive.$inject = [];
    function Directive() {
        // Usage:
        //
        // Creates:
        //
        var directive = {
            bindToController: true,
            controller: 'panierCtrl',
            controllerAs: 'panierCtrl',
            link: link,
            templateUrl:'views/panier.html',
            restrict: 'EAC',
            scope: {
                reduction:'=voucher'
            },
            replace:false
        };
        return directive;
        
        function link(scope, element, attrs) {
            element.css({
                color:'darkblue',
                backgroundColor:'rgb(128,128,128,0.5)'

            })
        }
    }
   
})();