(function() {
    'use strict';

    angular
        .module('app')
        .filter('mycurrency', Filter);

    function Filter() {
        return FilterFilter;

        ////////////////

        function FilterFilter(params,symbol,fractionSize){
            symbol=(undefined === symbol)?'$':symbol;
            fractionSize=(undefined === fractionSize)?2:fractionSize;

            var pow10=Math.pow(10,fractionSize);
            var arrondi=Math.round(params*pow10) / pow10;
            
            var str=''+arrondi;
            return str.replace('.',',')+symbol;
        }
    }
})();