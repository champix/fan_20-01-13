(function() {
    'use strict';

    angular
        .module('app')
        .controller('produitsCtrl', ControllerController);

    ControllerController.$inject = ['$scope','$http','produitSrvc'];
    function ControllerController($scope,$http,produitSrvc) {
        var thisProduits = this;
        this.produits=produitSrvc.produits;
        this.countProduits=produitSrvc.countProduits;
        
        
        // this.addProductToCart=produitSrvc.addProductToCart;

        $scope.onclickaddproducttocart=function (produit) {
            console.log(produit);
            produitSrvc.addProductToCart(produit);
        }

    }
})();