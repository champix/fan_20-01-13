(function() {
    'use strict';

    angular.module('soldes', ['ngRoute','app']).config(['$routeProvider',function(rp){
        rp.when('/soldes',{
            // template:'<h1>Soldes</h1>',//
            templateUrl:'views/produits.html',
            controller:'soldesCtrl',
            controllerAs:'produitsCtrl'
        })
    }])
    .filter('soldeOnly,',[function(){
        return function(arr){
            var retArr=[];
            arr.map((e,i)=>{
                if(undefined!==e.soldable || true===e.soldable){
                    retArr.push(e);
                }
            });
            return retArr;
            
        }
    }])
    .controller('soldesCtrl',SoldesCtrl);

    SoldesCtrl.$inject=['$scope','produitSrvc'];
    function SoldesCtrl($scope,produitsSrvc) {
        this.produits=produitsSrvc.produits;
    }

})();