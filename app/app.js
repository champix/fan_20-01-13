(function () {
    'use strict';
    var _debugRoute = false;
    angular.module('app', ['ngRoute','soldes'])
        .config(['$routeProvider',
            function ($routeProvider) {
                $routeProvider.when('/', {
                    template: '<h1>Bienvenu dans le nouveau site angular</h1>'
                })
                    .when('/produits', {
                        templateUrl: 'views/produits.html',
                        controller: 'produitsCtrl',
                        controllerAs: 'produitsCtrl'
                    })
                    .when('/produit', { redirectTo: '/produits' })
                    .when('/new/produit', { templateUrl: 'views/formProduit.html',controller:'produitCtrl' ,controllerAs:'produitCtrl' })
                    .when('/panier', { templateUrl: '/views/panier.html', controller: 'panierCtrl', controllerAs: 'panierCtrl' })
                    .when('/produit/:id', {
                        templateUrl: 'views/produit.html',
                        controller: 'produitCtrl',
                        controllerAs: 'produitCtrl'
                    })
                if (!_debugRoute) $routeProvider.otherwise({ redirectTo: '/' })
            }]);
})();
