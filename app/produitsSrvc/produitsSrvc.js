(function () {
    'use strict';
    const SRV_ADR='http://localhost:7500';
    angular
        .module('app')
        .service('produitSrvc', Service);

    Service.$inject = ['$http', '$routeParams', '$location'];
    function Service($http, $routeParams, $location) {
        var thisService=this;
        this.produits = []
        this.countProduits=0;
        this.produit={};
        this.panier=[];
        this.categories=[];
        
        this.totals={ht:0.0,tva:0.0};
        this.total2=0.0;
        const loadCat=function () {
            $http.get(SRV_ADR+'/categories').then((r)=>{
                thisService.categories.slice(1);
                r.data.map((e)=>thisService.categories.push(e))
            });
            
        };
        loadCat();


        this.loadProduit=function (id) {
            $http.get(SRV_ADR+'/produits/'+id+'?_expand=categorie')
                .then((response)=>{
                    console.log('success de loading : '+response.url,response.data)
                    thisService.produit=response.data;
                },(response)=>{console.log('echec de loading : '+response.url)})
        }
        this.addProductToCart=function (product) {
            if(undefined===product.quant){product.quant=1,      this.panier.push(product);
            }
            else{
                product.quant=product.quant+=1;
            }
            this.totals.ht+=product.prix;
            this.totals.tva+=(product.prix * 0.2);
            this.total2=this.total2+1;
            console.log(this)
        }
        const loadProduits=function () {
            $http.get(SRV_ADR+'/produits?_sort=nom&_expand=categorie')
            .then(function httpSuccess(response){
              console.log(response);
          
          
              thisService.produits.slice(1);
              response.data.map((e,i)=>thisService.produits.push(e))

              //console.log(thisService.produits)

             thisService.countProduits=thisService.produits.length; 
                },
                (response)=>{console.log('ca n\'a pas marcher',response)})

        }
        this.saveNewProduct=function () {
            $http({
                method:'POST',
                url:SRV_ADR+'/produits',
                headers: {
                    'Content-Type': 'application/json'
                  },
                  data: thisService.produit 
            }).then((response)=>{
                thisService.produits.push(response.data);
                $location.path('/produits').hash('produit-'+response.data.id)
            },
            (response)=>{

            });
        }
        
        loadProduits();

    }
})();