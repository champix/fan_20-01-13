(function() {
    'use strict';
    const SRV_ADR='http://localhost:7500';
    angular
        .module('app')
        .controller('produitCtrl', ProduitCtrl);
        
    ProduitCtrl.$inject=['$scope','$routeParams','produitSrvc'];
    function ProduitCtrl($scope,$routeParams,produitSrvc)
    {
        var thisProduit=this;
        this.cat=produitSrvc.categories;
      //  console.log($routeParams);
    this.produit=produitSrvc.produit;
    $scope.saveProduct=produitSrvc.saveNewProduct     
    produitSrvc.loadProduit($routeParams.id);
    
}
    
})();